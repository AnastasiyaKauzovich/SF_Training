public class AccountProcessor {

    @Future
    public static void countContacts(List<Id> accountIds) {
        List<Account> accounts = new List<Account>(
        [
                SELECT Id, Name, Number_of_Contacts__c, (SELECT Id FROM Contacts)
                FROM Account
                WHERE Id IN :accountIds
        ]);

        for (Account account: accounts) {
            account.Number_of_Contacts__c = account.Contacts.size();
        }
        update accounts;
    }
}