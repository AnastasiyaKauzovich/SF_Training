@IsTest
public class AccountProcessorTest {
    static Integer CONTACTS_PER_ACCOUNT = 2;

    @TestSetup
    static void setUp() {
        TestDataFactory.createAccountsWithContacts(5, CONTACTS_PER_ACCOUNT);
    }

    @IsTest
    static void testCountContacts() {
        Map<Id, Account> accounts = new Map<Id, Account>([
                SELECT Id, Name, Number_of_Contacts__c, (SELECT Id FROM Contacts)
                FROM Account
        ]);

        Test.startTest();
        AccountProcessor.countContacts(new List<Id>(accounts.keySet()));
        Test.stopTest();
        List< Account> updatedAccounts = new List<Account>([
                SELECT Number_of_Contacts__c
                FROM Account
        ]);
        for (Account account : updatedAccounts) {
            System.assertEquals(CONTACTS_PER_ACCOUNT, account.Number_of_Contacts__c);
        }
    }

}