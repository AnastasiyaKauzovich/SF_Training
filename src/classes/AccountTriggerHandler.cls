public without sharing class AccountTriggerHandler extends TriggerHandler {

    public override void onAfterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {

        List<Task> tasks = AccountTriggerHandlerHelper.createLinkedTasks(newObjects);
        insert tasks;
    }

}