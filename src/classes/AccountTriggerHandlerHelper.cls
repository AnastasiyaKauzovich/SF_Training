public without sharing class AccountTriggerHandlerHelper {

    public static List<Task> createLinkedTasks(List<SObject> newObjects) {
        List<Task> tasks = new List<Task>();

        for (SObject account : newObjects) {
            Task newTask = new Task();
            newTask.Subject = 'Negotiations with ' + ((Account) account).Name;
            newTask.Status = 'Open';
            newTask.Priority = 'Normal';
            newTask.WhatId = account.Id;
            tasks.add(newTask);
        }

        return tasks;
    }
}