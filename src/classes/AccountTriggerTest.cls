@IsTest
public class AccountTriggerTest {

    @TestSetup
    public static void setUp() {
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name = 'Account name1'));
        accounts.add(new Account(Name = 'Account name2'));

        insert accounts;
    }

    @IsTest
    public static void testTaskCreation() {
        Map<Id, Account> accountMap = new Map<Id, Account>(
        [SELECT Id, Name FROM Account WHERE Name LIKE 'Account name_']);

        List<Task> tasks = [
                SELECT AccountId, Id, Priority, Status, Subject, WhatId
                FROM Task
                WHERE WhatId IN :accountMap.keySet()
        ];

        Test.startTest();
        System.assert(tasks.size() == 2);
        for (Task task : tasks) {
            System.assertEquals('Negotiations with ' + accountMap.get(task.WhatId).Name, task.Subject);
        }
        Test.stopTest();
    }

    @IsTest
    public static void fakeTest() {
        Account newAccount = new Account(Name = 'Account name');

        insert newAccount;
        upsert newAccount;
        update newAccount;
        delete newAccount;
        undelete newAccount;
    }
}