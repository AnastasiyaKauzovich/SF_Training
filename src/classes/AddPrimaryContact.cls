public class AddPrimaryContact implements Queueable {
    private Contact contact;
    private String state;

    public AddPrimaryContact(Contact contact, String stateAbbreviation) {
        this.contact = contact;
        this.state = stateAbbreviation;
    }


    public void execute(QueueableContext param1) {
        List<Account> accounts = new List<Account>([
                SELECT Id
                FROM Account
                WHERE BillingState = :state
                LIMIT 200
        ]);
        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            Contact newContact = contact.clone(
                    false, false, true, false);
            newContact.AccountId = account.Id;
            contacts.add(newContact);
        }
        insert contacts;
    }
}