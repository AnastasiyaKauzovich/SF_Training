@IsTest
public class AddPrimaryContactTest {
    static String TEST_BILLING_STATE = 'CA';

    @TestSetup
    static void setUp() {
        TestDataFactory.createAccounts(50, 'NY');
        TestDataFactory.createAccounts(50, TEST_BILLING_STATE);
        Contact testContact = new Contact(LastName = 'Last name');
        insert testContact;
    }

    @IsTest
    static void testPrimaryAdd() {
        Contact contact = [SELECT LastName FROM Contact LIMIT 1];
        AddPrimaryContact addPrimaryContact = new AddPrimaryContact(contact, TEST_BILLING_STATE);
        Test.startTest();
        System.enqueueJob(addPrimaryContact);
        Test.stopTest();

        List<Account> resultAccounts = new List<Account>([
                SELECT (SELECT LastName FROM Contacts)
                FROM Account
                WHERE BillingState = :TEST_BILLING_STATE
        ]);
        for (Account account : resultAccounts) {
            System.assertEquals(contact.LastName, account.Contacts.get(0).LastName);
        }
    }
}