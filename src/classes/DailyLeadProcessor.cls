public class DailyLeadProcessor implements Schedulable {

    static String DEFAULT_LEAD_SOURCE = 'Dreamforce';

    public void execute(SchedulableContext schedulableContext) {
        List<Lead> leads = new List<Lead>([SELECT LeadSource FROM Lead WHERE LeadSource = '' LIMIT 200]);

        for (Lead lead : leads) {
            lead.LeadSource = DEFAULT_LEAD_SOURCE;
        }
        update leads;
    }
}