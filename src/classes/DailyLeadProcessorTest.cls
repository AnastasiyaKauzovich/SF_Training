@IsTest
public with sharing class DailyLeadProcessorTest {

    static String DEFAULT_LEAD_SOURCE = 'Dreamforce';
    public static String CRON_EXP = '0 0 0 ? * * *';

    @TestSetup
    static void setUp() {
        TestDataFactory.createLeads(200);
    }

    @IsTest
    static void testDailyLeadProcessor() {
        Test.startTest();
        DailyLeadProcessor dailyLeadProcessor = new DailyLeadProcessor();
        System.schedule('job name', CRON_EXP, dailyLeadProcessor);
        Test.stopTest();

        List<Lead> leads = new List<Lead>([SELECT LeadSource FROM Lead WHERE LeadSource = '' LIMIT 200]);
        for (Lead lead : leads) {
            System.assertEquals(DEFAULT_LEAD_SOURCE, lead.LeadSource);
        }
    }
}