public class LeadProcessor implements Database.Batchable<SObject> {
    static String DEFAULT_LEAD_SOURCE = 'Dreamforce';

    public Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return Database.getQueryLocator(
                'SELECT Id, LeadSource FROM Lead'
        );
    }

    public void execute(Database.BatchableContext batchableContext, List<Lead> leads) {
        for (Lead lead : leads) {
            lead.LeadSource = DEFAULT_LEAD_SOURCE;
        }
        update leads;
    }

    public void finish(Database.BatchableContext batchableContext) {
    }
}