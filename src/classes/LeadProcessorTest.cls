@IsTest
public class LeadProcessorTest {
    static String DEFAULT_LEAD_SOURCE = 'Dreamforce';

    @TestSetup
    static void setUp() {
        TestDataFactory.createLeads(200);
    }

    @IsTest
    static void testLeadBatchProcessor() {
        Test.startTest();
        LeadProcessor leadProcessor = new LeadProcessor();
        Database.executeBatch(leadProcessor);
        Test.stopTest();
        List<Lead> leads = new List<Lead>([SELECT LeadSource FROM Lead]);
        for (Lead lead : leads) {
            System.assertEquals(DEFAULT_LEAD_SOURCE, lead.LeadSource);
        }
    }
}