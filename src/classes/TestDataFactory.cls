@IsTest
public class TestDataFactory {

    public static List<Account> createAccounts(Integer accountsAmount, String billingState) {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < accountsAmount; i++) {
            Account newAccount = new Account(Name = 'Account ' + i, BillingState = billingState);
            accounts.add(newAccount);
        }
        insert accounts;
        return accounts;
    }

    public static List<Lead> createLeads(Integer leadsAmount) {
        List<Lead> leads = new List<Lead>();

        for (Integer i = 0; i < leadsAmount; i++) {
            Lead lead = new Lead(LastName = 'Last name' + i, Company = 'Company' + i);
            leads.add(lead);
        }
        insert leads;
        return leads;
    }

    public static List<Account> createAccountsWithContacts(Integer accountsAmount, Integer contactsPerAccount) {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < accountsAmount; i++) {
            Account newAccount = new Account(Name = 'Account ' + i);
            accounts.add(newAccount);
        }
        insert accounts;

        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            for (Integer i = 0; i < contactsPerAccount; i++) {
                Contact newContact = new Contact(LastName = 'Last name ' + 1, AccountId = account.Id);
                contacts.add(newContact);
            }
        }
        insert contacts;
        return accounts;
    }


}